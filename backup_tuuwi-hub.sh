today=$(date +"%Y-%m-%d")
mkdir -p tuuwi-hub/db tuuwi-hub/files
echo "----- create database dump -----------------"
ssh tuuwi-hub 'mysqldump -u root --password= --opt hub1 > ~/hub1.sql'
echo "----- compress database dump ---------------"
ssh tuuwi-hub 'gzip ~/hub1.sql'
echo "----- download database dump ---------------"
scp tuuwi-hub:hub1.sql.gz tuuwi-hub/db/humhub-${today}.sql.gz
echo "----- delete database dump on server -------"
ssh tuuwi-hub 'rm ~/hub1.sql.gz'
echo "----- compress hub files --------------------"
# provide progress with the help of pv => only approx. as only size of uploads is meassured
#ssh tuuwi-hub 'cd /var/www/vhosts/h2845994.stratoserver.net/tuuwi-hub.de/ && tar cf - protected/modules protected/config uploads | pv -s $(du -sb uploads | awk `{print $1}`) | gzip > ~/files.tar.gz'
ssh tuuwi-hub 'cd /var/www/vhosts/h2845994.stratoserver.net/tuuwi-hub.de/ && tar czf ~/files.tar.gz protected/modules protected/config uploads'
echo "----- download compressed hub files --------"
scp tuuwi-hub:files.tar.gz tuuwi-hub/files.tar.gz
echo "----- delete compressed files on sever -----"
ssh tuuwi-hub 'rm ~/files.tar.gz'
#scp -r -v tuuwi-hub:/var/www/vhosts/h2845994.stratoserver.net/tuuwi-hub.de/protected/modules/ tuuwi-hub/
#scp -r -v tuuwi-hub:/var/www/vhosts/h2845994.stratoserver.net/tuuwi-hub.de/protected/config/ tuuwi-hub/
#scp -r -v tuuwi-hub:/var/www/vhosts/h2845994.stratoserver.net/tuuwi-hub.de/uploads/ tuuwi-hub/
