today=$(date +"%Y-%m-%d")
mkdir -p tuuwi-agdsn/etherpad/
echo "### Creating databasedump of etherpad.sql on server ###"
ssh tuuwi-agdsn 'sudo mysqldump -u root --password= --opt etherpad > /data/backup/etherpad.sql'
echo "### Compressing databasedump for download ###"
ssh tuuwi-agdsn 'gzip /data/backup/etherpad.sql'
echo "### Downloading databasedump ###"
scp tuuwi-agdsn:/data/backup/etherpad.sql.gz tuuwi-agdsn/etherpad/etherpad-${today}.sql.gz
#rsync -az --suffix=`date +'.%y%m%d%H%M'` tuuwi-agdsn:/data/etherpad.sql.gz tuuwi-agdsn/etherpad/
echo "### Deleting dump on server ###"
ssh tuuwi-agdsn 'rm /data/backup/etherpad.sql.gz'
echo "### Downloading etherpad settings ###"
scp tuuwi-agdsn:/opt/etherpad/etherpad-lite/settings.json tuuwi-agdsn/etherpad/
echo "### Done ###"
#scp -v tuuwi-agdsn:/etc/mumble-server.ini tuuwi-agdsn/
#scp -v tuuwi-agdsn:/var/lib/mumble-server/mumble-server.sqlite tuuwi-agdsn/
#scp -r -v tuuwi-agdsn:/home/music/botamusique/ tuuwi-agdsn/
