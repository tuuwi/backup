# Backup

## Prerequisites

- Install sshpass using your favorite package manager
- Make sure your ssh config matches the [config file](./config). You find your ssh config in "~/.ssh/config".
- Setup disk encryption on a your drive using luks. You can find a guide how to do this [here](https://lobotuerto.com/blog/how-to-setup-full-disk-encryption-on-a-secondary-hdd-in-linux/).
- Set the executable flag for all shell scripts.

```shell
chmod +x *.sh
```

## Usage

- Find your block storage partition using "lsblk".
- Open the LUKS partition, change "sdd1" to the desired partition and mount the partition.

```shell
mkdir -p mnt
sudo cryptsetup luksOpen /dev/sdd1 encrypteddrive
sudo mount /dev/mapper/encrypteddrive mnt/
```

- Start the backup_all.sh script with the admin password for tuuwi.de

```shell
sh backup_all.sh root_password
```

- Once the script is done, you can unmount and close the partition.

```shell
sudo umount /dev/mapper/encrypteddrive
sudo cryptsetup luksClose /dev/mapper/encrypteddrive
```

## Restore information

Restore mysql dumps:

```shell
mysql -u root -p newdatabase < /path/to/newdatabase.sql
```
